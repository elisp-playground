#!/usr/bin/env python3

import os
import subprocess

import bottle
from bottle import Bottle, request, response

TIMEOUT = 10
HOST = 'https://elisp-playground.herokuapp.com/'
SOURCE = 'https://depp.brause.cc/elisp-playground/'
PORT = int(os.getenv('PORT', '8000'))
DEBUG = os.getenv('DEBUG')


class StealthyBottle(Bottle):
    def default_error_handler(self, res):
        print('Error {}: {}'.format(res.status_code, res.body))
        return ''


def eval_elisp(code):
    args = ['emacs', '-Q', '--batch', '--eval',
            '(let ((print-escape-newlines t))'
            '  (princ (prin1-to-string (progn {}))))'.format(code)]
    p = subprocess.run(args, capture_output=True, timeout=TIMEOUT)
    return {'status': p.returncode,
            'stdout': p.stdout.decode('utf-8'),
            'stderr': p.stderr.decode('utf-8')}


app = StealthyBottle()


@app.route('/', method=['GET', 'POST'])
def index():
    if request.method == 'GET':
        response.content_type = 'text/plain'
        return "curl --data '(+ 1 1)' {}\n\nSource: {}\n".format(HOST, SOURCE)
    try:
        return eval_elisp(request.body.read().decode('utf-8'))
    except FileNotFoundError:
        return {'status': 'ENOENT'}
    except subprocess.TimeoutExpired:
        return {'status': 'ETIMEDOUT'}


if __name__ == '__main__':
    bottle.run(app, port=PORT, debug=DEBUG)
